Rails.application.routes.draw do

  #root
  root 'users#index'

  # Users
  match '/signup', :to => 'users#create', :via => [:post], :as => :create_user
  match '/update', :to => 'users#update', :via => [:patch], :as => :update_user
  get '/signup' => 'users#new', as: :signup
  get '/me' => 'users#show', as: :show_user
  get '/update' => 'users#edit', as: :edit_user
  get '/validate_user', :to => "users#validate_user"

  # Sessions
  match '/login', :to => 'sessions#create', :via => [:post], :as => :login_user
  get '/login' => 'sessions#new', as: :login
  get '/logout' => 'sessions#destroy', as: :logout

  # Password Reset
  get '/forgot-password' => 'password_reset#new', as: :forgot_password
  match '/forgot-password', :to => 'password_reset#verify_username', :via => [:post], :as => :verify_username
  get '/forgot-password/1' => 'password_reset#new'
  match '/forgot-password/1', :to => 'password_reset#verify_security_answer', :via => [:post], :as => :verify_security_answer
  get '/forgot-password/reset-password' => 'password_reset#new'
  match '/forgot-password/reset-password', :to => 'password_reset#reset_password', :via => [:patch], :as => :reset_password

  # Albums and assets
  resources :albums do
    resources :assets
    resources :filters do
      resources :tags
    end
  end

  #Filters and tags
  get '/albums/:album_id/distill' => 'filters#show', as: :distill_album
  get '/albums/:album_id/distilled_albums' => 'filters#distilled_albums', as: :distilled_album
  get '/albums/:album_id/distilled_albums/:tag_id' => 'filters#assets_with_tag', as: :distilled_assets
  get '/albums/:album_id/distilled_albums/:tag_id/download' => 'albums#download', as: :download_album

  match '/albums/:album_id/filters/add_filter_to_album', :to => 'filters#add_filter_to_album', :via => [:post], :as => :add_filter_to_album
  match '/albums/:album_id/filters/remove_filter_from_album', :to => 'filters#remove_filter_from_album', :via => [:post], :as => :remove_filter_from_album
  match '/tags/assign_tag_to_asset', :to => 'tags#assign_tag_to_asset', :via => [:post], :as => :assign_tag_to_asset

  get '/albums/:album_id/asset/:asset_id/tags' => "assets#tags", as: :asset_tags
  get '/assets/asset_belongs_to_current_user' => "assets#asset_belongs_to_current_user", as: :asset_belongs_to_current_user


  #Shared Albums
  #resources :shared_albums
  match '/shared_albums/share_album', :to => 'shared_albums#share_album', :via => [:post], :as => :share_album
  get '/shared_album/:shared_album_id' => 'shared_albums#show', as: :shared_album
  match '/shared_albums/delete_album', :to => 'shared_albums#destroy', :via => [:post], :as => :delete_share_album

end

module SessionsHelper

  #Pass in any material icon name from https://www.google.com/design/icons/
  def icon (icon_name,text="",classes="")
    %Q|<i class="material-icons prefix #{classes}">#{icon_name}</i> #{text}|
  end

end

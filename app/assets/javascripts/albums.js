// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/
var ready = function() {


  $(".delete-album-button").click(function() {
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this album!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it.",
      cancelButtonText: "No, cancel."
    }, function(isConfirm) {
      if (isConfirm) {
        $(".delete-album").click();
      }
    });
  });

  if ($('.image-uploader-modal').length > 0) {
    Dropzone.autoDiscover = false;

    var dropzone = new Dropzone(".dropzone", {
      paramName: "asset[image]", // Rails expects the file upload to be something like model[field_name]
      addRemoveLinks: false // Don't show remove links on dropzone itself.
    });
  }

  $(".best_in_place").best_in_place();


  $('.mosaicflow').mosaicflow();
  $(window).resize();

  $('#image-uploader-modal').on('hidden.bs.modal', function(e) {
    location.reload();
  });


  initModal();


};

$(document).ready(ready);
$(document).on("page:load", ready);


initModal = function() {

  $('.image-thumbnail').click(function() {
    clicked_image_obj = this;
    $.when(showModal()).done(function(x) {
      showPhoto(clicked_image_obj);
    });
    return false;
  });

  $("#sg-overlay-modal-close-button").click(function() {
    $("#sg-overlay-modal").trigger("close");
  });

  $("#sg-overlay-modal-delete-button").click(function() {
      asset_path = $(".sg-overlay-modal-current-image").data("asset-path");

      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file.",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
      }, function() {
         deleteAsset(asset_path);
      });
  });

  //Triggers once the image is loaded
  $('#sg-overlay-modal-image').on('load', function() {
    hide_loader();
  });

  //Triggers once the modal is about to close
  $("#sg-overlay-modal").on("close", function(event) {
    $("#sg-overlay-modal").fadeOut(function() {
      $("a").removeClass('sg-overlay-modal-current-image');
      hideModal();
    });
  });


  //Asset Navigation
  $("#next-button").click(function() {
    nextImage();
  });
  $("#previous-button").click(function() {
    prevImage();
  });

  $('body').keyup(function (event) {
    if($( "a" ).hasClass( "sg-overlay-modal-current-image" )) {
      e = event.keyCode;
      switch(e) {
        case 37:
          prevImage();
          break;
        case 39:
          nextImage();
          break;
        case 27:
          $("#sg-overlay-modal").trigger("close");
          break;
      }
    }
  });

  //Utility Functions

  function showModal() {
    $("#sg-overlay-modal").hide();
    $("#sg-overlay-modal").css("visibility", "visible");
    $("#sg-overlay-modal").fadeIn('25', function() {
      $('body').css('overflow', 'hidden');
      setup_loading_gif();
    });
  }

  function hideModal() {
    $("#sg-overlay-modal-image").attr("src", '');
    $('body').css('overflow', '');
    $("#sg-overlay-modal").trigger("closed");
  }

  function showPhoto(obj) {
    show_loader();
    image_source = $(obj).data('image');
    $(obj).addClass("sg-overlay-modal-current-image");
    $("#sg-overlay-modal-image").attr("src", image_source);
    removeDeleteButtonIfNotSupported();
    refreshTagsView();
  }

  function nextImage() {
    current_image_obj = $(".sg-overlay-modal-current-image");
    current_image_obj_data_count = $(current_image_obj).data("asset-counter");
    current_image_obj_data_count = (current_image_obj_data_count + 1).toString();
    next_image_obj = $('*[data-asset-counter="' + current_image_obj_data_count + '"]');
    if(next_image_obj.length != 0) {
      $(current_image_obj).removeClass("sg-overlay-modal-current-image");
      showPhoto(next_image_obj);
    }
  }


  function prevImage() {
    current_image_obj = $(".sg-overlay-modal-current-image");
    current_image_obj_data_count = $(current_image_obj).data("asset-counter");
    current_image_obj_data_count = (current_image_obj_data_count - 1).toString();
    previous_image_obj = $('*[data-asset-counter="' + current_image_obj_data_count + '"]');
    if(previous_image_obj.length != 0) {
      $(current_image_obj).removeClass("sg-overlay-modal-current-image");
      showPhoto(previous_image_obj);
    }
  }



  function deleteAsset(path) {
    $.ajax({
      type: "POST",
      url: path,
      dataType: "json",
      data: {
        "_method": "delete"
      },
      success: function(data) {
        location.reload();
      }
    });
  }

  function setup_loading_gif(){
    $("#image-loader-gif").center();
    $("#image-loader-gif").css("visibility", "visible");
    $("#image-loader-gif").hide();
  }
  function show_loader(){
    $("#image-loader-gif").show();
  }
  function hide_loader(){
    $("#image-loader-gif").hide();
  }
  if($("#albums.show").length > 0) {
    initTagging();
  }
}




initTagging = function(){

  var reserved_keycodes = [27,37,39];
  var currentAlbumId = $("#album-assets-container").data("album-id");
  var filter_data, tags_data, valid_keywords = [];

  $.ajax({
    type: "get",
    url: "/albums/"+currentAlbumId+"/filters",
    dataType: "json",
    success: function(data) {
      tags_data = process_filters_data(data);
      tags_data.forEach(function(tag) {
          valid_keywords.push(tag.keyboard_shortcut);
      })
    }
  });

  $('body').keyup(function (event) {

    keyword_pressed = String.fromCharCode(event.which);
    keycode_pressed = event.keyCode;

    var is_modal_open = $( "a" ).hasClass("sg-overlay-modal-current-image");
    var did_not_press_reserved_key = ($.inArray(keycode_pressed, reserved_keycodes)) == -1;
    var keyword_is_valid = ($.inArray(keyword_pressed, valid_keywords)) > -1;

    if(is_modal_open && did_not_press_reserved_key && keyword_is_valid ) {
        var tag = getTag(keyword_pressed, tags_data);
        var currentAssetId = $(".sg-overlay-modal-current-image").data('asset-id');
        showVisualFeedbackForKeyword(tag.name);
        add_tag_to_asset(currentAssetId,tag.tag_id,tag.filter_id);
    }
  });

  function getTag(keyword,data) {
    //[{filter_id:x, keyboard_shortcut:y, name:z, tag_id:y},.....] will return tag for a given keyword from the array of tag data
    var tag_to_return;
    data.forEach(function(tag){
      if(tag.keyboard_shortcut == keyword) {
        tag_to_return = tag;
      }
    });
    return tag_to_return;
  }

  function process_filters_data(data) {
      var filter_data = data;
      var tagsData = [];
      filter_data.forEach(function(filter) {
          filter.tags.forEach(function(tag) {
             current_tag = {filter_id:filter.id,tag_id:tag.id,name:tag.name,keyboard_shortcut:tag.keyboardShortcut}
             tagsData.push(current_tag);
          });
      });
      return tagsData;
  }


  function add_tag_to_asset (asset_id,tag_id,filter_id) {
    $.ajax({
      type: "POST",
      url: "/tags/assign_tag_to_asset",
      dataType: "json",
      data: {
        "asset_id": asset_id,
        "filter_id": filter_id,
        "tag_id": tag_id
      },
      success: function(data) {
          console.log("Tagged");
          refreshTagsView();
      }
    });
  }
}

function doesAssetBelongToLoggedInUserWithCallback(asset_id, callback, args, condition){
  $.ajax({
    type: "get",
    url: "/assets/asset_belongs_to_current_user",
    dataType: "json",
    data: {
      "asset_id": asset_id
    },
    success: function(data) {
        if(data.message == "yes" && condition == true) {
          if(callback && args){
            callback(args[0], args[1]);
          }
        } else {
          if(condition == false && data.message == "no") {
            callback();
          }
        }
    }
  });
}

function removeDeleteButtonIfNotSupported () {
  current_image_obj = $(".sg-overlay-modal-current-image");
  asset_id = $(current_image_obj).data("asset-id");
  doesAssetBelongToLoggedInUserWithCallback(asset_id, removeDeleteButton, [], false);
}

function removeDeleteButton(){
  $("#sg-overlay-modal-delete-button").remove();
}

function refreshTagsView () {
  obj =  $(".sg-overlay-modal-current-image");
  args = [$(obj).data("album-id"),$(obj).data("asset-id")];
  doesAssetBelongToLoggedInUserWithCallback($(obj).data("asset-id"), getAssetInfoForAlbum, args, true );
}


function getAssetInfoForAlbum(album_id, tag_id) {
  $.ajax({
    type: "get",
    url: "/albums/"+album_id+"/asset/"+tag_id+"/tags",
    dataType: "json",
    success: function(data) {
        updateModalWithTags(data);
    }
  });
}

function updateModalWithTags(tags_data) {
  tags = [];
  html_to_append = "";

  tags_data.forEach(function(tag) {
    tags.push(tag.name);
  });

  tags.forEach(function(tag){
    html_to_append += '<li><a href="#" class="tag">'+tag+'</a></li>';
  });

  $("#tags-list").html(html_to_append);
}


function showVisualFeedbackForKeyword(keyword) {
      $("#visual_feedback_for_tag").html(keyword).css({fontSize : "1em",opacity: "1",color: "white"});
    	$( "#visual_feedback_for_tag" ).animate({
    		fontSize:"20em",
    		opacity: 0.0,
        color: "black"
    	}, 700 );
}


jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
    return this;
}

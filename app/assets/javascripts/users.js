// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/

var ready = function() {

  $("#create-album-button").on("click", function(event) {
      if($('#album_title').val().length == 0 || $('#album_title').val().length > 30){
          $("#album_title").css("border","1px solid red");
          $("#album_title").focus();
      } else {
        $(".create-album-form").submit();
      }
  });


  $('.album-thumbnail').each(function(i, obj) {
      backgroundImage = $(obj).data('image');
      $(obj).css("background-image",'url('+backgroundImage+')');
  });

  $("#user-avatar-edit-button").on("click", function(event) {
    $('input[type=file]#user_avatar').trigger('click');
  });

  $("input[type=file]#user_avatar").change(function(){
    readURL(this);
  });

};

$(document).ready(ready);
$(document).on("page:load", ready);


// Functions

function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#user-avatar').attr('src', e.target.result).css("opacity",".5");
          $('#user-avatar-edit-button').css("background-color","#2ecc71")
      }
      reader.readAsDataURL(input.files[0]);
  }
}

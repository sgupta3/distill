var ready;

ready = function() {
    $('form').on('click', '.remove_fields', function(event) {
      $(this).prev('input[type=hidden]').val('1');
      $(this).closest('.fieldset').hide();
      return event.preventDefault();
    });

   $('form').on('click', '.add_fields', function(event) {
      var regexp, time;
      time = new Date().getTime();
      regexp = new RegExp($(this).data('id'), 'g');
      $(this).before($(this).data('fields').replace(regexp, time));
      return event.preventDefault();
    });

    $(".filters-selection-checkboxes").change(function(){
      filter_id = $(this).attr('value');

      if($(this).is(':checked')){
        add_filter_to_album(filter_id);
      } else {
        remove_filter_from_album(filter_id);
      }
    });

    $("#add_tag").click(function(e){
        $("a.add_fields").click();
        return event.preventDefault();
    });

    $("#album-share-button").click(function(e){
      e.preventDefault();
      var input = $("#album-share-username").val();
      var album_id = $("#album-share-username").data("album-id");
      var tag_id = $("#album-share-username").data("tag-id");
      validate_user_and_share(input,album_id,tag_id);
    });

    $(".delete-shared-album").click(function(){
      container = $(this).parent();
      $.ajax({
        type: "post",
        url: "/shared_albums/delete_album",
        dataType: "json",
        data: {
          "shared_album_id": $(this).data("shared-album-id")
        }
      }).always(function(data){
        if(data.message == "success") {
          container.remove();
        } else {
          display_error_on_share_form("Couldn't unshare!");
        }
      });
    });

    $('#album-share-modal').on('hidden.bs.modal', function(e) {
      $("#album-share-error").html("");
      $("#album-share-username").val("");
    });
};

function validate_user_and_share(username,album_id,tag_id) {
  is_valid = false;
  $.ajax({
    type: "get",
    url: "/validate_user",
    dataType: "json",
    data: {
      "username": username
    }
  }).always(function(data){
    if(data.message == "success") {
      share_album(album_id,tag_id,username);
    } else {
      display_error_on_share_form("The username is valid!");
    }
  });
}

function share_album(album_id, tag_id, username) {
  $.ajax({
    type: "post",
    url: "/shared_albums/share_album",
    dataType: "json",
    data: {
      "username": username,
      "album_id": album_id,
      "tag_id": tag_id
    }
  }).always(function(data){
      if(data.message == "success") {
        $("#album-share-error").html("");
        $("#album-share-username").val("");
        $('#album-share-modal').modal('hide')
        location.reload();
      } else {
        display_error_on_share_form("You can't share this album with the username provided.");
      }
  });
}

function display_error_on_share_form(message) {
  $("#album-share-error").html(message);
}

function add_filter_to_album(filter_id) {
  $.ajax({
    type: "POST",
    url: "filters/add_filter_to_album",
    dataType: "json",
    data: {
      "filter_id": filter_id
    },
    success: function(data) {
      console.log("Filter added");
    }
  });
}

function remove_filter_from_album(filter_id) {
  $.ajax({
    type: "POST",
    url: "filters/remove_filter_from_album",
    dataType: "json",
    data: {
      "filter_id": filter_id
    },
    success: function(data) {
      console.log("Filter removed");
    }
  });
}

$(document).ready(ready);

$(document).on('page:load', ready);

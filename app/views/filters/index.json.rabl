collection @filters
attributes :id, :name

child :tags do
  attributes :id,:name,:keyboardShortcut
end

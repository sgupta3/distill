class Album < ActiveRecord::Base
  belongs_to :user
  has_many :assets, dependent: :destroy
  has_and_belongs_to_many :filters
end

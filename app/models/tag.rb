class Tag < ActiveRecord::Base
  belongs_to :filter
  has_and_belongs_to_many :assets
  validates :keyboardShortcut, uniqueness: { case_sensitive: false, message: "The keyboard shortcut - '%{value}' has already been taken by another tag."}

end

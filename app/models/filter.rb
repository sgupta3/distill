class Filter < ActiveRecord::Base
  belongs_to :user
  has_many :tags, dependent: :destroy
  has_and_belongs_to_many :albums
  accepts_nested_attributes_for :tags, allow_destroy: :true
end

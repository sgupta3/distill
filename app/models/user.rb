require "open-uri"

class User < ActiveRecord::Base
  attr_accessor :password, :fullname
  before_save :encrypt_password, :update_user_avatar

  validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  validates_presence_of :first_name, :last_name, :username, :email, :security_question, :security_answer
  validates_uniqueness_of :email, :username

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: ""
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  has_many :albums, dependent: :destroy
  has_many :filters, dependent: :destroy
  has_many :sharedAlbums, dependent: :destroy

  def self.authenticate (username,password)
    user = find_by_username(username)
    if user && password_matches?(user,password)
      user
    end
  end

  def fullname
    "#{first_name} #{last_name}"
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def update_user_avatar
    unless avatar?
      uri = URI.encode("http://avatarly.herokuapp.com/avatar?size=300&text=#{fullname}")
      self.avatar = URI.parse(uri)
      self.avatar_file_name = "avatar.jpeg"
    end
  end

  private
  def self.password_matches?(user,password_supplied)
     user.password_hash == BCrypt::Engine.hash_secret(password_supplied, user.password_salt)
  end
end

class Asset < ActiveRecord::Base

  belongs_to :album
  has_and_belongs_to_many :tags

  has_attached_file :image, :styles => { :large => "800x800>", :medium => "300x300>", :small => "250x160#", :thumb => "100x100>" }

  validates_attachment 	:image,
        :presence => true,
        :content_type => { :content_type => /\Aimage\/.*\Z/ }

  def self.with_tag(t)
    assets =  []
    Asset.all.each {|a| assets << a if a.tags.include?(t) }
    return assets
  end
end

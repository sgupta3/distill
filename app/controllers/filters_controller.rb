class FiltersController < ApplicationController

  def index
    album = current_user.albums.find(params[:album_id]);
    @filters = album.filters
  end

  def new
    @album = current_user.albums.find(params[:album_id])
    @filter = current_user.filters.new
    @filter.tags.build

  end

  def show
    @album = current_user.albums.find(params[:album_id])
    @filters = current_user.filters
  end

  def create
    @filter = current_user.filters.new(filter_params)
    @album = current_user.albums.find(params[:album_id])
    if @filter.save
      redirect_to distill_album_path(@album)
    else
      render 'new'
    end
  end

  def edit
    @filter = current_user.filters.find(params[:id])
    @album = current_user.albums.find(params[:album_id])
  end

  def update
    @filter = current_user.filters.find(params[:id])
    @album = current_user.albums.find(params[:album_id])

    if @filter.update(filter_params)
      redirect_to distill_album_path(@album)
    else
      render 'edit'
    end
  end

  def distilled_albums
    @album = current_user.albums.find(params[:album_id])
    @filters = []
    @album.assets.each do |a|
      a.tags.each do |t|
        if !@filters.include?(t.filter)
          @filters << t.filter
        end
      end
    end
  end

  def assets_with_tag
    @album = current_user.albums.find(params[:album_id])
    @tag = Tag.find(params[:tag_id])
    @assets = @album.assets.with_tag(@tag)
    @shared_instances =  SharedAlbum.where(album_id: @album.id, tag_id: @tag.id)
  end

  def add_filter_to_album
    filter = current_user.filters.find(params[:filter_id])
    album = current_user.albums.find(params[:album_id])
    if (album && filter)
      unless album.filters.include?(filter)
        album.filters << filter
      end
      render json: { message: "success" }, :status => 200
    else
      render json: { error: 'Sorry, couldn\'t add the filter'}, :status => 400
    end
  end

  def remove_filter_from_album
    filter = current_user.filters.find(params[:filter_id])
    album = current_user.albums.find(params[:album_id])
    if(album && filter)
      album.filters.delete(filter)
      render json: { message: "success" }, :status => 200
    else
      render json: { error: 'Sorry, couldn\'t delete the filter'}, :status => 400
    end
  end

  private
  def find_filter
    @filter = current_user.filters.find(params[:id])
  end

  def filter_params
    params.require(:filter).permit(:name, tags_attributes: [:id, :name, :folderName, :keyboardShortcut, :_destroy])
  end

  def shared_with_users
    shared_instances = SharedAlbum.where(album_id: @album.id, tag_id: @tag.id)
    # users_shared_with = []
    # shared_instances.each do |s|
    #   user = User.find_by_id(s.shared_with)
    #   if user
    #       users_shared_with  << user
    #   end
    # end
    # return users_shared_with
  end
end

class UsersController < ApplicationController
  before_action :require_login, :only => [:edit, :update]
  def index
    if current_user
      @albums = current_user.albums.all
      @album = current_user.albums.new
      @shared_albums = shared_albums_with_current_user
      render :show
    else
      redirect_to login_path
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to root_url, :flash => { :success => "Congratulations! You have successfully signed up." }
    else
      render 'new'
    end
  end

  def show
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update(edit_user_params)
      flash.now[:success] = "Updated Successfully!"
    else
      flash.now[:info] = "Update Unsuccessful!"
    end
    render 'edit'
  end

  def validate_user
    user = User.find_by_username(params[:username])
    if user
      render json: { message: "success" }, :status => 200
    else
      render json: { error: 'Sorry, no user found'}, :status => 400
    end
  end

  private
  def user_params
   params[:user].permit(:first_name, :last_name, :username, :password, :password_confirmation, :email, :security_question, :security_answer)
  end

  def edit_user_params
    params[:user].permit(:avatar, :first_name, :last_name, :password, :password_confirmation, :email, :security_question, :security_answer)
  end

  def require_login
    if !current_user
      redirect_to root_url
    end
  end

end

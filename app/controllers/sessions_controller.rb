class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.authenticate(params[:username], params[:password])
    if user
      session[:user_id] = user.id
      redirect_to root_url
    else
      flash.now[:danger] = "Oops! Looks like you entered invalid email or password. Lets try again!"
      render 'new'
    end
  end

  def destroy
    if session[:user_id]
      session[:user_id] = nil
    end
    redirect_to root_url
  end
end

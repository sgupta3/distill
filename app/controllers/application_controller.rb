class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def shared_albums_with_current_user
    shared_albums =   SharedAlbum.where(shared_with: current_user.id)
    shared_albums.each do |shared_album|
      tag = Tag.find_by_id(shared_album.tag_id)
      album = Album.find_by_id(shared_album.album_id)
      if !(tag && album)
        shared_albums = shared_albums.delete(shared_album)
      end
    end
    shared_albums
  end

end

class PasswordResetController < ApplicationController
  def new
  end

  def verify_username
    @user = User.find_by_username(params[:username])
    if @user
      render 'prompt_security_question'
    else
      flash.now[:danger] = "Uh oh! Looks like there is no account with the username you entered."
      render 'new'
    end
  end

  def verify_security_answer
    @user = User.find_by_username(params[:username])
    if !@user
      redirect_to :forgot_password
      return
    end

    if @user.security_answer == params[:security_answer]
      render 'password_reset'
    else
      flash.now[:danger] = "Uh oh! The answer is incorrect."
      render 'prompt_security_question'
    end
  end

  def reset_password
    @user = User.find_by_username(params[:username])
    if @user.update(edit_user_params)
      redirect_to root_url, :flash => { :success => "Password has been successfully reset!" }
    else
      flash.now[:info] = "Update Unsuccessful!"
      render 'password_reset'
    end
  end


  private
    def edit_user_params
      params[:user].permit(:password, :password_confirmation)
    end

end

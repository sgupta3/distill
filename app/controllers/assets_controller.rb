class AssetsController < ApplicationController

  def new
    @asset = current_album.assets.new
  end

  def create
  	@asset = current_album.assets.new(upload_params)
  	if @asset.save
  	  render json: { message: "success" }, :status => 200
  	else
  	  render json: { error: 'Sorry, couldn\'t upload the image.'}, :status => 400
  	end
  end

  def destroy
      @asset = current_album.assets.find(params[:id])
      if @asset.destroy
        render json: { message: "success" }, :status => 200
      else
        render json: { error: 'Sorry, couldn\'t delete the file'}, :status => 400
      end
  end

  def tags
    @tags = current_album.assets.find(params[:asset_id]).tags
  end

  def asset_belongs_to_current_user
    asset = Asset.find_by_id(params[:asset_id])
    if asset.album.user == current_user
      render json: { message: "yes" }, :status => 200
    else
      render json: { message: "no" }, :status => 200
    end
  end

  private
  def upload_params
    params.require(:asset).permit(:image)
  end

  def current_album
    current_user.albums.find(params[:album_id])
  end

end

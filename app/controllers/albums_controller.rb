class AlbumsController < ApplicationController
  before_action :require_login

  def new
  end

  def create
    @album = current_user.albums.create(album_params)
    redirect_to @album
  end

  def show
    @album = current_user.albums.find_by_id( params[:id])
    @assets = @album.assets
    @asset = current_album.assets.new
  end

  def edit
    @album = current_user.albums.find_by_id( params[:id])
  end

  def update
    @album = current_user.albums.find_by_id( params[:id])
    @album.update(album_params)
    respond_to do |format|
      format.json { render json: @album }
    end
  end

  def destroy
    @album = current_user.albums.find_by_id(params[:id])
    @album.destroy
    redirect_to root_url
  end

  def download
    album = current_user.albums.find_by_id(params[:album_id])
    tag = Tag.find(params[:tag_id]) unless params[:tag_id] == "all"

    if(tag && album)
      assets = album.assets.with_tag(tag)
    else
      assets = album.assets
    end

    t = Tempfile.new('tmp-zip-' + request.remote_ip)
    Zip::ZipOutputStream.open(t.path) do |zos|
      assets.each do |asset|
        zos.put_next_entry(asset.image_file_name)
        zos.print IO.read(asset.image.path)
      end
    end

    file_name = album.title
    if tag
      file_name = file_name + '-' + tag.name
    end
    send_file t.path, :type => "application/zip", :filename => file_name + ".zip"

    t.close
  end

  private
    def album_params
			params.require(:album).permit(:title)
		end

		def require_login
			if current_user == nil
        redirect_to '/login'
      end
    end

    def current_album
      current_user.albums.find(params[:id])
    end

end

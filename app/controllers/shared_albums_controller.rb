class SharedAlbumsController < ApplicationController
  def index
    @shared_albums = shared_albums_with_current_user
  end

  def show
    shared_album = SharedAlbum.find_by_id(params[:shared_album_id])
    if (shared_albums_with_current_user.include?(shared_album))
      @assets = Album.find(shared_album.album_id).assets.with_tag(Tag.find(shared_album.tag_id))
      @album = Album.find(shared_album.album_id)
      @shared_album = shared_album
    end
  end

  def destroy
    shared_album = SharedAlbum.find_by_id(params[:shared_album_id])
    if shared_album.destroy
      render json: { message: "success" }, :status => 200
    else
      render json: { error: 'Sorry, couldn\'t delete the shared album'}, :status => 400
    end
  end

  def share_album
    user = User.find_by_username(params[:username]);
    album = current_user.albums.find(params[:album_id]);
    tag = Tag.find(params[:tag_id]);
        if (user && album && tag && (user != current_user) && !already_shared(user,album,tag))
          sharedAlbum = current_user.sharedAlbums.new
          sharedAlbum.tag_id = tag.id
          sharedAlbum.album_id = album.id
          sharedAlbum.shared_with = user.id
          if(sharedAlbum.save)
            render json: { message: "success" }, :status => 200
          else
            render json: { error: 'error'}, :status => 400
          end
        else
          render json: { error: 'Sorry, no user, album or tag found'}, :status => 400
        end
      end

private
  def already_shared(user,album,tag)
    sharedAlbum = SharedAlbum.where(shared_with: user.id, album_id: album.id, tag_id: tag.id)
    sharedAlbum.exists?
  end
end

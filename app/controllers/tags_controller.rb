class TagsController < ApplicationController
  def new
  end

  def assign_tag_to_asset
    filter = current_user.filters.find(params[:filter_id])
    tag = filter.tags.find(params[:tag_id])
    asset = Asset.find(params[:asset_id])

    if(tag && asset)
        remove_tag_of_same_filter_if_exists(asset,tag,filter)
        asset.tags << tag
        render json: { message: "success" }, :status => 200
    else
      render json: { error: 'Sorry, couldn\'t add the tag'}, :status => 400
    end
  end


  private
  def remove_tag_of_same_filter_if_exists (asset,tag,filter)
      asset.tags.each do |t|
        if t.filter == filter
            remove_tag_from_asset(asset,t)
        end
      end
  end

  def remove_tag_from_asset (asset,tag)
      asset.tags.delete(tag) if asset.tags.include?(tag)
  end
end

# Distill #
### Revolutionary way to sort your photos! ###
---
As a photographer and a frequent traveller, I usually have more than 200 photos for one day. It becomes very messy and hard when I am trying to go through all the pictures and choosing the good ones. The story doesn’t end there for me. It’s not only about choosing the good pictures but also organizing them in different categories. For instance, I want to choose all the pictures that:

* I want to upload on Facebook
* I want to send to my parents
* I want to print for photo frames
* I want to use for lots of other different things.

The question is how do I do that without going over the pictures multiple times and still have them in different folders? There is no service online that I can use for that. So I decided to create a web application that would let you upload your album and then lets you create sort filters to be able to sort your photos using different criteria. Once the sorting is done, the app would also allow you to download only the files that you want. For example: All the photos that you want to upload to Facebook. The application would also save all your photos in your account so that you can access them anywhere anytime.
class AddFilterToTags < ActiveRecord::Migration
  def change
    add_reference :tags, :filter, index: true, foreign_key: true
  end
end

class RemoveSharedAtFromSharedAlbums < ActiveRecord::Migration
  def change
    remove_column :shared_albums, :shared_at, :datetime
  end
end

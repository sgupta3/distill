class CreateAlbumsFiltersJoin < ActiveRecord::Migration
  def change
    create_table :albums_filters, :id => false do |t|
      t.integer "album_id"
      t.integer "filter_id"
    end
    add_index :albums_filters, ["album_id","filter_id"]
  end
end

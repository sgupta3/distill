class CreateSharedAlbums < ActiveRecord::Migration
  def change
    create_table :shared_albums do |t|
      t.integer :user_id
      t.integer :tag_id
      t.integer :album_id
      t.integer :shared_with
      t.datetime :shared_at

      t.timestamps null: false
    end
  end
end

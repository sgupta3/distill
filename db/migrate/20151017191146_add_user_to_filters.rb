class AddUserToFilters < ActiveRecord::Migration
  def change
    add_reference :filters, :user, index: true, foreign_key: true
  end
end

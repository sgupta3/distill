class RemoveWidthAndHeightFromAsset < ActiveRecord::Migration
  def change
    remove_column :assets, :width, :integer
    remove_column :assets, :height, :integer
  end
end

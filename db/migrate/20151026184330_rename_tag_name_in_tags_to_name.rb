class RenameTagNameInTagsToName < ActiveRecord::Migration
  def change
    rename_column :tags, :tagName, :name
  end
end

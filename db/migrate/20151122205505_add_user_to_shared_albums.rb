class AddUserToSharedAlbums < ActiveRecord::Migration
  def change
    add_reference :shared_albums, :user, index: true, foreign_key: true
  end
end

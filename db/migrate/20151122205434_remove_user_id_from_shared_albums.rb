class RemoveUserIdFromSharedAlbums < ActiveRecord::Migration
  def change
    remove_column :shared_albums, :user_id, :integer
  end
end

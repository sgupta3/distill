class CreateAssetsTags < ActiveRecord::Migration
  def change
    create_table :assets_tags, :id => false  do |t|
      t.integer "asset_id"
      t.integer "tag_id"
    end
    add_index :assets_tags, ["asset_id","tag_id"]
  end
end

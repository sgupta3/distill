class AddAlbumToAssets < ActiveRecord::Migration
  def change
    add_reference :assets, :album, index: true, foreign_key: true
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151123015208) do

  create_table "albums", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "albums", ["user_id"], name: "index_albums_on_user_id"

  create_table "albums_filters", id: false, force: :cascade do |t|
    t.integer "album_id"
    t.integer "filter_id"
  end

  add_index "albums_filters", ["album_id", "filter_id"], name: "index_albums_filters_on_album_id_and_filter_id"

  create_table "assets", force: :cascade do |t|
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "album_id"
  end

  add_index "assets", ["album_id"], name: "index_assets_on_album_id"

  create_table "assets_tags", id: false, force: :cascade do |t|
    t.integer "asset_id"
    t.integer "tag_id"
  end

  add_index "assets_tags", ["asset_id", "tag_id"], name: "index_assets_tags_on_asset_id_and_tag_id"

  create_table "filters", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  add_index "filters", ["user_id"], name: "index_filters_on_user_id"

  create_table "shared_albums", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "album_id"
    t.integer  "shared_with"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
  end

  add_index "shared_albums", ["user_id"], name: "index_shared_albums_on_user_id"

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.string   "folderName"
    t.string   "keyboardShortcut"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "filter_id"
    t.integer  "user_id"
  end

  add_index "tags", ["filter_id"], name: "index_tags_on_filter_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "username"
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "security_question"
    t.string   "security_answer"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

end
